title: $:/plugins/fk/soundcite/usage
created: 20200610000000000
modified: 20200613000000000

!! `soundcite` macro example

The `soundcite` macro takes fives parameters of which four are optional. The `url` may be either relative, e. g. `files/Beethoven.ogg`, or absolute, e. g. `https://example.org/files/Beethoven.ogg`.

Embedding it as follows...

```
<<soundcite url:"https://upload.wikimedia.org/wikipedia/commons/e/ee/Beet5mov1bars1to5.ogg" text:"This">> is only the beginning.
```

...yields...

<<soundcite url:"https://upload.wikimedia.org/wikipedia/commons/e/ee/Beet5mov1bars1to5.ogg" text:"This">> is only the beginning.

!! `soundcite` macro parameters

|!#|!Parameter|!Description|!Default|
|1|url|URL to MP3, M4A, WAV or Ogg audio file|(none)|
|2|text|overlayed text|`Play`|
|3|plays|number of loops|`1`|
|4|start|start time in milliseconds|`0`|
|5|end|end time in milliseconds|(stop playback at the end)|

!! Tips & tricks

//according to the SoundCite website//

* Keep it brief! Long clips distract from the story.
* Select your link text carefully. Try not to use so much text that it wraps to two lines.
* Remember you don't have to cut up your audio file. SoundCite can serve multiple segments from a single source.

!! Pause-all button

The `soundcite-pause-all-button` macro provides a button that pauses everything when clicked:

```
<<soundcite-pause-all-button>>
```

...yields...

<<soundcite-pause-all-button>>

To add the button to your page toolbar, write the following to a tiddler tagged `$:/tags/PageControls`:

```
@@font-size:0.5em;vertical-align: middle;<<soundcite-pause-all-button>>@@
```
